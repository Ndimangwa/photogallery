package tz.co.zoomtong.photogallery.api

import retrofit2.Call
import retrofit2.http.GET
//Key: 243037a6e5c5014a0176b3ba48811c99
//Secret: ad535bd1ad3189ea
//Path: https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=243037a6e5c5014a0176b3ba48811c99&format=json&nojsoncallback=1&extras=url_s
interface FlickrApi {
    @GET(
        "services/rest/?method=flickr.interestingness.getList" +
                "&api_key=243037a6e5c5014a0176b3ba48811c99" +
                "&format=json" +
                "&nojsoncallback=1" +
                "&extras=url_s"
    )
    fun fetchPhotos(): Call<FlickrResponse>
}