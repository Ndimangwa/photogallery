package tz.co.zoomtong.photogallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import tz.co.zoomtong.photogallery.api.GalleryItem

class PhotoGalleryViewModel: ViewModel() {
    val galleryItemLiveData: LiveData<List<GalleryItem>>

    init {
        galleryItemLiveData = FlickrFetchr().fetchPhotos()
    }
}